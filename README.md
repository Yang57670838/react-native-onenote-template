## tech choice
1. redux for temporary state management
2. SQLite for offline database storage
3. TypeScript for typing

## TODO
1. configure path from relative path to path alias
2. add unit test and automation test
3. switch to better css framework such as Tailwinds
4. add eslint, stylelint, husky, etc... to maintain local code quality
5. add spinner when deal with database
