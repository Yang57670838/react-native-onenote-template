import { useEffect, useState } from "react";
import { StyleSheet, View, SafeAreaView } from "react-native";
import { StatusBar } from "expo-status-bar";
import { useFonts } from "expo-font";
import { NavigationContainer } from "@react-navigation/native";
import { Provider } from "react-redux";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { RootStackNavigatorList } from "./src/types/nav";
import { store } from "./src/store";
import { init } from "./src/db/db";
import AppLoading from "expo-app-loading";
import Categories from "./src/screens/Categories";
import Notes from "./src/screens/Notes";
import Login from "./src/screens/Login";

const Stack = createNativeStackNavigator<RootStackNavigatorList>();

export default function App() {
  const [dbInitialized, setDbInitialized] = useState<boolean>(false);
  const [fontLoaded] = useFonts({
    "open-sans": require("./assets/fonts/OpenSans-Regular.ttf"),
    "open-sans-bold": require("./assets/fonts/OpenSans-Bold.ttf"),
  });

  useEffect(() => {
    init()
      .then(() => {
        setDbInitialized(true);
      })
      .catch((err) => {
        console.error(err);
      });
  }, []);

  if (!fontLoaded || !dbInitialized) {
    return <AppLoading />;
  }
  return (
    <View style={styles.appScreen}>
      <StatusBar style="auto" />
      <SafeAreaView style={styles.mainContent}>
        <Provider store={store}>
          <NavigationContainer>
            <Stack.Navigator
              initialRouteName="Login"
              screenOptions={{
                headerStyle: { backgroundColor: "#351401" },
                headerTintColor: "white",
                contentStyle: { backgroundColor: "#3f2f25" },
              }}
            >
              <Stack.Screen
                name="Categories"
                component={Categories}
                options={{
                  title: "Categories",
                }}
              />
              <Stack.Screen
                name="Notes"
                component={Notes}
                options={{
                  title: "Notes",
                }}
              />
              <Stack.Screen
                name="Login"
                component={Login}
                options={{
                  title: "Login",
                }}
              />
            </Stack.Navigator>
          </NavigationContainer>
        </Provider>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  appScreen: {
    flex: 1,
  },
  mainContent: {
    flex: 1,
    justifyContent: "center",
  },
});
