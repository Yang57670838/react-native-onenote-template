import { StyleSheet, View } from "react-native";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigation } from "@react-navigation/native";
import { CategoryScreenNavigationProp } from "../../types/nav";
import { appActions } from "../../store/slices/app";
import PrimaryButton from "../../components/Button/PrimaryButton";
import Color from "../../constants/color";
import AppText from "../../components/Text/AppText";
import categories from "../../mocks/categories.json";

const Categories = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation<CategoryScreenNavigationProp>();

  const selectCategoryHandler = (category: string) => {
    dispatch(appActions.selectCategory(category));
    navigation.navigate("Notes");
  };

  return (
    <>
      <View style={styles.loginBox}>
        {categories.map((category) => {
          return (
            <PrimaryButton
              pressHandler={() => selectCategoryHandler(category.name)}
              key={category.id}
            >
              <View style={styles.btnBox}>
                <AppText style={styles.btnText} bold={false}>
                  {category.name}
                </AppText>
              </View>
            </PrimaryButton>
          );
        })}
      </View>
    </>
  );
};

export default Categories;

const styles = StyleSheet.create({
  loginBox: {
    marginTop: 50,
    marginHorizontal: 24,
    padding: 16,
    backgroundColor: "#004165",
    borderRadius: 8,
    elevation: 4,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 6,
    shadowOpacity: 0.25,
  },
  loginInput: {
    height: 50,
    fontSize: 22,
    borderBottomColor: Color.primary,
    borderBottomWidth: 2,
    color: Color.primary,
    marginVertical: 8,
    textAlign: "center",
    fontFamily: "open-sans",
  },
  headingContainer: {
    marginHorizontal: 24,
  },
  btnBox: {
    flexDirection: "row",
  },
  btnText: {
    marginRight: 5,
    color: "white",
  },
  forgetCredentialsText: {
    textDecorationLine: "underline",
  },
});
