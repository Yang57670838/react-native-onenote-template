import { StyleSheet, View } from "react-native";
import { useDispatch } from "react-redux";
import { useState } from "react";
import { useNavigation } from "@react-navigation/native";
import { LoginScreenNavigationProp } from "../../types/nav";
import { appActions } from "../../store/slices/app";
import PrimaryButton from "../../components/Button/PrimaryButton";
import Color from "../../constants/color";
import AppText from "../../components/Text/AppText";
import users from "../../mocks/users.json";

const Login = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation<LoginScreenNavigationProp>();

  const signinHandler = (username: string) => {
    dispatch(appActions.selectUser(username));
    navigation.navigate("Categories");
  };

  return (
    <>
      <View style={styles.loginBox}>
        {users.map((user) => {
          return (
            <PrimaryButton
              pressHandler={() => signinHandler(user.username)}
              key={user.id}
            >
              <View style={styles.btnBox}>
                <AppText style={styles.btnText} bold={false}>
                  {user.username}
                </AppText>
              </View>
            </PrimaryButton>
          );
        })}
      </View>
    </>
  );
};

export default Login;

const styles = StyleSheet.create({
  loginBox: {
    marginTop: 50,
    marginHorizontal: 24,
    padding: 16,
    backgroundColor: "#004165",
    borderRadius: 8,
    elevation: 4,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 6,
    shadowOpacity: 0.25,
  },
  loginInput: {
    height: 50,
    fontSize: 22,
    borderBottomColor: Color.primary,
    borderBottomWidth: 2,
    color: Color.primary,
    marginVertical: 8,
    textAlign: "center",
    fontFamily: "open-sans",
  },
  headingContainer: {
    marginHorizontal: 24,
  },
  btnBox: {
    flexDirection: "row",
  },
  btnText: {
    marginRight: 5,
    color: "white",
  },
  forgetCredentialsText: {
    textDecorationLine: "underline",
  },
});
