import { StyleSheet, View, Modal, Button, TextInput } from "react-native";
import { FC, useState } from "react";

export type OwnProps = {
  id: string;
  updateHandler: (id: string, value: string) => void;
  value: string;
  isShow: boolean;
  setModalStatus: (status: boolean) => void;
};

const EditNoteModal: FC<OwnProps> = ({
  id,
  updateHandler,
  value,
  isShow,
  setModalStatus,
}) => {
  const [currentValue, updateValue] = useState<string>(value);

  const closeModal = () => {
    setModalStatus(false);
  };

  const updateNotes = (text: string) => {
    updateValue(text);
  };

  const updateNoteInDBHandler = () => {
    if (currentValue) {
      updateHandler(id, currentValue);
    }
  }

  return (
    <Modal animationType="slide" visible={isShow}>
      <View style={styles.modalContainer}>
        <View style={styles.inputContainer}>
          <TextInput
            placeholder="Add Notes"
            style={styles.textInput}
            onChangeText={updateNotes}
            value={currentValue}
          />
          <Button title="Update" onPress={updateNoteInDBHandler} />
        </View>
        <View>
          <Button title="Close" onPress={closeModal} />
        </View>
      </View>
    </Modal>
  );
};

export default EditNoteModal;

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    paddingTop: 50,
    paddingHorizontal: 16,
    justifyContent: "space-around",
  },
  inputContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    flex: 1,
    alignItems: "center",
    marginBottom: 24,
    borderBottomWidth: 1,
    borderBottomColor: "#cccccc",
  },
  textInput: {
    borderWidth: 1,
    borderColor: "#007dba",
    borderRadius: 6,
    backgroundColor: "#e4d0ff",
    color: "#120438",
    width: "70%",
    padding: 8,
    marginRight: 8,
    fontFamily: "open-sans",
  },
});
