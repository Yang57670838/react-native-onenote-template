import { StyleSheet, View, Button, TextInput, FlatList } from "react-native";
import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../store";
import { fetchNotes } from "../../db/services/fetchNotes";
import { insertOneNote } from "../../db/services/insertOneNote";
import { deleteOneNote } from "../../db/services/deleteOneNote";
import { updateOneNote } from "../../db/services/updateOneNote";
import { Note } from "../../types/notes";
import Color from "../../constants/color";
import AppText from "../../components/Text/AppText";
import OneNote from "./OneNote";

const Notes = () => {
  const { app: appStore } = useSelector((state: RootState) => state);
  const [newNotes, updateNewNotes] = useState<string>("");
  const [allNotes, setAllNotes] = useState<Array<Note>>([]);

  useEffect(() => {
    let unmounted = false;
    const fetchData = async () => {
      const response = await fetchNotes({
        user: appStore.user as string,
        category: appStore.category as string,
      });
      console.log("fetch res", response);
      if (!unmounted) {
        setAllNotes(response);
      }
    };
    fetchData();
    return () => {
      unmounted = true;
    };
  }, []);

  const updateNotes = (text: string) => {
    updateNewNotes(text);
  };

  const deleteItem = async (id: string) => {
    console.log("deleting item", id);
    try {
      await deleteOneNote({
        id,
      });
      const filtered = allNotes.filter((item) => item.id !== id);
      setAllNotes(filtered);
    } catch (err) {
      console.error(err);
    }
  };

  const addNewNotes = async () => {
    try {
      if (newNotes) {
        const newPayload = {
          user: appStore.user as string,
          category: appStore.category as string,
          value: newNotes,
        };
        await insertOneNote(newPayload);
        setAllNotes([
          ...allNotes,
          {
            ...newPayload,
            id: "id",
          },
        ]);
      }
    } catch (err) {
      console.error(err);
    }
  };

  const updateHandler = async (id: string, value: string) => {
    console.log("updating one note", id);
    try {
      await updateOneNote({
        id,
        value,
      });
      const updated = allNotes.map((item) => {
        if (item.id === id) {
          return {
            ...item,
            value,
          };
        }
        return item;
      });
      setAllNotes(updated);
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <>
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          <TextInput
            placeholder="Add Notes"
            style={styles.textInput}
            onChangeText={updateNotes}
          />
          <Button title="Add" onPress={addNewNotes} />
        </View>
        <View style={styles.listContainer}>
          <FlatList
            keyExtractor={(item) => item.id}
            data={allNotes}
            renderItem={(item) => {
              return (
                <OneNote
                  note={item.item}
                  deleteItem={deleteItem}
                  updateHandler={updateHandler}
                />
              );
            }}
            alwaysBounceVertical={false}
          />
        </View>
      </View>
    </>
  );
};

export default Notes;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50,
    paddingHorizontal: 16,
  },
  inputContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    flex: 1,
    alignItems: "center",
    marginBottom: 24,
    borderBottomWidth: 1,
    borderBottomColor: "#cccccc",
  },
  textInput: {
    borderWidth: 1,
    borderColor: "#007dba",
    borderRadius: 6,
    backgroundColor: "#e4d0ff",
    color: "#120438",
    width: "70%",
    padding: 8,
    marginRight: 8,
    fontFamily: "open-sans",
  },
  listContainer: {
    flex: 5,
  },
});
