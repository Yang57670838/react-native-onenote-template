import { StyleSheet, View, Pressable } from "react-native";
import { FC, useState } from "react";
import { AntDesign } from "@expo/vector-icons";
import { Note } from "../../types/notes";
import AppText from "../../components/Text/AppText";
import Color from "../../constants/color";
import EditNoteModal from "./EditNoteModal";

export type OwnProps = {
  note: Note;
  deleteItem: (id: string) => void;
  updateHandler: (id: string, value: string) => void;
};

const OneNote: FC<OwnProps> = ({ note, deleteItem, updateHandler }) => {
  const [modalStatus, setModalStatus] = useState<boolean>(false);
  const editItem = () => {
    setModalStatus(true);
  };

  const update = (id: string, value: string) => {
    // TODO: success update in db then close modal
    setModalStatus(false);
    updateHandler(id, value);
  };
  return (
    <>
      <View style={styles.container}>
        <AppText style={styles.text} bold={false}>
          {note.value}
        </AppText>
        <View style={styles.iconsContainer}>
          <Pressable onPress={editItem} style={styles.editIcon}>
            <AntDesign name="edit" size={24} color="green" />
          </Pressable>
          <Pressable onPress={() => deleteItem(note.id)}>
            <AntDesign name="delete" size={24} color="red" />
          </Pressable>
        </View>
      </View>
      <EditNoteModal
        id={note.id}
        updateHandler={update}
        value={note.value}
        isShow={modalStatus}
        setModalStatus={setModalStatus}
      />
    </>
  );
};

export default OneNote;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: 8,
    padding: 12,
    borderRadius: 6,
    backgroundColor: Color.listBoxBackground,
    elevation: 4,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3,
  },
  text: {
    color: "white",
    padding: 8,
  },
  iconsContainer: {
    flexDirection: "row",
    padding: 3,
  },
  editIcon: {
    marginRight: 20,
  },
});
