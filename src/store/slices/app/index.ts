import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AppStore } from "../../../types/app";

const initialState: AppStore = {
  user: undefined,
  category: undefined,
};

const appSlice = createSlice({
  name: "app",
  initialState: initialState,
  reducers: {
    selectUser: (state, action: PayloadAction<string>) => {
      state.user = action.payload;
    },
    selectCategory: (state, action: PayloadAction<string>) => {
      state.category = action.payload;
    },
  },
});

export const appActions = appSlice.actions;
export default appSlice.reducer;