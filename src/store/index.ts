import { configureStore, combineReducers } from "@reduxjs/toolkit";
import appSlice from "./slices/app";

const combinedReducer = combineReducers({
  app: appSlice,
});

export const store = configureStore({
  reducer: combinedReducer,
});

export type RootState = ReturnType<typeof combinedReducer>;
