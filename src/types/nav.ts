import type { NativeStackNavigationProp } from "@react-navigation/native-stack";
import type { RouteProp } from "@react-navigation/native";

export type RootStackNavigatorList = {
  Login: undefined;
  Categories: undefined;
  Notes: undefined;
};

export type LoginScreenNavigationProp = NativeStackNavigationProp<
  RootStackNavigatorList,
  "Categories"
>;

export type CategoryScreenNavigationProp = NativeStackNavigationProp<
  RootStackNavigatorList,
  "Notes"
>;
