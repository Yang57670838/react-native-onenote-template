export interface Note {
  id: string;
  value: string;
  user: string;
  category: string;
}
