import { database } from "../db";

export interface UpdateOneNoteArg {
  id: string;
  value: string;
}

export function updateOneNote(arg: UpdateOneNoteArg) {
  return new Promise<void>((resolve, reject) => {
    database.transaction((tx) => {
      tx.executeSql(
        `UPDATE notes SET value = ? WHERE id = ?`,
        [arg.value, arg.id],
        (_, result) => {
          resolve();
          if (result.rowsAffected > 0) {
            console.log("updated");
          }
        },
        (_, err): boolean | any => reject(err)
      );
    });
  });
}
