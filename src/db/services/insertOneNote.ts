import { database } from "../db";

export interface InsertOneNoteArg {
  category: string;
  user: string;
  value: string;
}

export function insertOneNote(arg: InsertOneNoteArg) {
  const promise = new Promise((resolve, reject) => {
    database.transaction((tx) => {
      tx.executeSql(
        `INSERT INTO notes (value, user, category) VALUES (?, ?, ?)`,
        [arg.value, arg.user, arg.category],
        (_, result) => {
          console.log(result);
          resolve(result);
        },
        (_, err): boolean | any => reject(err)
      );
    });
  });
}
