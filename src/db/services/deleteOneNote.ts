import { database } from "../db";

export interface DeleteOneNoteArg {
  id: string;
}

export function deleteOneNote(arg: DeleteOneNoteArg) {
  return new Promise<void>((resolve, reject) => {
    database.transaction((tx) => {
      tx.executeSql(
        `DELETE FROM notes WHERE id = ?`,
        [arg.id],
        (_, result) => {
          resolve();
          if (result.rowsAffected > 0) {
            console.log("deleted");
          }
        },
        (_, err): boolean | any => reject(err)
      );
    });
  });
}
