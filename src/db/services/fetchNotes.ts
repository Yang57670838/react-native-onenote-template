import { database } from "../db";
import { Note } from "../../types/notes";

export interface FetchNotesFilterPayload {
  category: string;
  user: string;
}

export function fetchNotes(arg: FetchNotesFilterPayload) {
  return new Promise<Array<Note>>((resolve, reject) => {
    database.transaction((tx) => {
      tx.executeSql(
        `SELECT * from notes WHERE user = ? AND category = ?`,
        [arg.user, arg.category],
        (_, result) => {
          console.log(result);
          resolve(result.rows._array);
        },
        (_, err): boolean | any => reject(err)
      );
    });
  });
}
