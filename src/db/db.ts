import * as SQLite from "expo-sqlite";

export const database = SQLite.openDatabase("local.notes.db");

export function init() {
  return new Promise<void>((resolve, reject) => {
    database.transaction((tx) => {
      tx.executeSql(
        "CREATE TABLE IF NOT EXISTS notes (id INTEGER PRIMARY KEY AUTOINCREMENT, value NOT NULL, user NOT NULL, category NOT NULL)",
        [],
        () => resolve(),
        (_, err): boolean | any => reject(err)
      );
    });
  });
}
